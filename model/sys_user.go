package model

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

type SysUser struct {
	gorm.Model
	UUID      uuid.UUID `json:"uuid"`
	Username  string    `json:"userName" gorm:"column:user_name"`
	Password  string    `json:"-" gorm:"column:pass_word"`
	NickName  string    `json:"nickName" gorm:"default:'QMPlusUser'"`
	HeaderImg string    `json:"headerImg" gorm:"default:'http://www.henrongyi.top/avatar/lufu.jpg'"`
}

// 设置User的表名为`sys_users_test`
func (s SysUser) TableName() string {
	return "sys_users_test"
}
