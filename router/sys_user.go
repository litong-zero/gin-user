package router

import (
	"gin-test/api/v1"
	"github.com/gin-gonic/gin"
)

func InitUserRouter(Router *gin.RouterGroup) {
	UserRouter := Router.Group("user")
	{
		UserRouter.GET("", v1.GetUserList)           // 分页获取用户列表
		UserRouter.GET(":id", v1.GetUser)           // 分页获取用户列表
		UserRouter.POST("", v1.AddUser)           // 分页获取用户列表
		UserRouter.PUT(":id", v1.UpdateUser)           // 分页获取用户列表
		UserRouter.DELETE(":id", v1.DeleteUser)           // 分页获取用户列表
	}
}
