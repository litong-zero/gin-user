# Gin-user
> 一个Gin框架的联手demo、包括增删改查等等。
> 只是API接口，没有前端页面。

## 1.使用技术
 - Gin (v1.6.3)
 - Gorm (v1.9.12)
 - Mysql (v1.5.0)
 - ...
 
## 2.项目结构目录
```
│  postman_collection.json  // postman配置文件
│  db.sql  // 数据库表结构
│  config.yaml  // 配置文件
│  go.mod      // 包依赖配置
│  main.go    // 程序入口
├─api
│  └─v1     // api 方法
│          sys_user.go
│          
├─config    // 配置文件结构体
│      config.go
│      
├─core     
│      config.go // 配置文件读取
│      server.go // 启动HTTP server服务
│      
├─global
│  │  global.go  // 全局共享对象
│  │  
│  └─response
│          response.go // 公共返回结构体
│          
├─initialize
│      db_table.go  // 注册数据库表
│      mysql.go  // 初始化mysql连接池
│      router.go // 初始化总路由
│      
├─middleware
│      cors.go  // 跨域处理配置
│      
├─model
│  │  sys_user.go  // 数据库映射结构体
│  │  
│  ├─request
│  │      common.go  // 公共请求参数
│  │      sys_user.go  //  用户请求参数
│  │      
│  └─response
│          common.go   // 公共相应参数
│          sys_user.go  //  用户相应参数
├─router
│      sys_user.go  // 用户业务路由
│      
├─service
│      sys_user.go   // 用户业务处理层
│      
└─utils
        md5.go   // 工具类
```