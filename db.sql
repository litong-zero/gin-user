CREATE TABLE `sys_users_test`
(
    `id`             int(10) unsigned NOT NULL AUTO_INCREMENT,
    `created_at`     timestamp        NULL DEFAULT NULL,
    `updated_at`     timestamp        NULL DEFAULT NULL,
    `deleted_at`     timestamp        NULL DEFAULT NULL,
    `uuid`           varbinary(255)        DEFAULT NULL,
    `user_name`      varchar(255)          DEFAULT NULL,
    `pass_word`      varchar(255)          DEFAULT NULL,
    `nick_name`      varchar(255)          DEFAULT 'QMPlusUser',
    `header_img`     varchar(255)          DEFAULT 'http://www.henrongyi.top/avatar/lufu.jpg',
    `authority_id`   varchar(255)          DEFAULT '888',
    `authority_name` varchar(255)          DEFAULT NULL,
    `username`       varchar(255)          DEFAULT NULL,
    `password`       varchar(255)          DEFAULT NULL,
    `phone_data`     varchar(255)          DEFAULT NULL,
    `manager`        varchar(255)          DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_users_deleted_at` (`deleted_at`) USING BTREE,
    KEY `idx_sys_users_deleted_at` (`deleted_at`) USING BTREE,
    KEY `idx_sys_users_test_deleted_at` (`deleted_at`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 16
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;