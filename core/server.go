package core

import (
	"fmt"
	"gin-test/global"
	"gin-test/initialize"
	"net/http"
	"time"
)

func RunWindowsServer() {

	Router := initialize.Routers()
	address := fmt.Sprintf(":%d", global.GVA_CONFIG.System.Addr)
	s := &http.Server{
		Addr:           address,
		Handler:        Router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	// 保证文本顺序输出
	time.Sleep(10 * time.Microsecond)
	println("server run success on ", address)

	println(s.ListenAndServe())
}
