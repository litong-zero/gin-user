package initialize

import (
	"gin-test/middleware"
	"gin-test/router"
	"github.com/gin-gonic/gin"
)

//初始化总路由

func Routers() *gin.Engine {
	var Router = gin.Default()
	//Router.Use(middleware.LoadTls())  // 打开就能玩https了
	println("use middleware logger")
	// 跨域
	Router.Use(middleware.Cors())
	println("use middleware cors")
	// 方便统一添加路由组前缀 多服务器上线使用
	ApiGroup := Router.Group("")

	router.InitUserRouter(ApiGroup) // 注册用户路由

	println("router register success")
	return Router
}
