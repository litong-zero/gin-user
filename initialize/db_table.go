package initialize

import (
	"gin-test/global"
	"gin-test/model"
)

//注册数据库表专用
func DBTables() {
	db := global.GVA_DB
	db.AutoMigrate(
		model.SysUser{},
	)
	println("register table success")
}
