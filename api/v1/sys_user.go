package v1

import (
	"fmt"
	"gin-test/global/response"
	"gin-test/model"
	"gin-test/model/request"
	resp "gin-test/model/response"
	"gin-test/service"
	"github.com/gin-gonic/gin"
	"strconv"
)

// @Tags SysUser
// @Summary 分页获取用户列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "分页获取用户列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /user/getUserList [post]
func GetUserList(c *gin.Context) {
	var pageInfo request.PageInfo

	pageStr := c.Query("page")
	page, err := strconv.ParseInt(pageStr, 10, 64)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("page格式错误，%v", err), c)
		return
	}

	pageSizeStr := c.Query("pageSize")
	pageSize, err := strconv.ParseInt(pageSizeStr, 10, 64)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("pageSize格式错误，%v", err), c)
		return
	}

	pageInfo.Page = int(page)
	pageInfo.PageSize = int(pageSize)

	err, list, total := service.GetUserInfoList(pageInfo)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.OkWithData(resp.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, c)
	}
}

func GetUser(c *gin.Context) {

	idVal, ok := c.Params.Get("id")
	if !ok {
		response.FailWithMessage(fmt.Sprintf("id格式错误"), c)
		return
	}
	id, err := strconv.ParseUint(idVal, 10, 64)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("id格式错误，%v", err), c)
		return
	}

	err, user := service.GetUserById(uint(id))
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.OkWithData(user, c)
	}
}

func AddUser(c *gin.Context) {
	var addUser model.SysUser
	_ = c.ShouldBindJSON(&addUser)

	err, user := service.AddUser(addUser)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.OkWithData(user, c)
	}
}

func UpdateUser(c *gin.Context) {
	var updateUser model.SysUser
	_ = c.ShouldBindJSON(&updateUser)

	idVal, ok := c.Params.Get("id")
	if !ok {
		response.FailWithMessage(fmt.Sprintf("id格式错误"), c)
		return
	}
	id, err := strconv.ParseUint(idVal, 10, 64)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("id格式错误，%v", err), c)
		return
	}


	err, user := service.UpdateUser(uint(id), updateUser)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.OkWithData(user, c)
	}
}

func DeleteUser(c *gin.Context) {
	idVal, ok := c.Params.Get("id")
	if !ok {
		response.FailWithMessage(fmt.Sprintf("id格式错误"), c)
		return
	}
	id, err := strconv.ParseUint(idVal, 10, 64)
	if err != nil {
		response.FailWithMessage(fmt.Sprintf("id格式错误，%v", err), c)
		return
	}
	errSer := service.DeleteUser(uint(id))
	if errSer != nil {
		response.FailWithMessage(fmt.Sprintf("获取数据失败，%v", err), c)
	} else {
		response.Ok(c)
	}
}
