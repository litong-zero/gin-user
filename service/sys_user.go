package service

import (
	"gin-test/global"
	"gin-test/model"
	"gin-test/model/request"
	"gin-test/utils"
	uuid "github.com/satori/go.uuid"
)

// @title    GetInfoList
// @description   get user list by pagination, 分页获取数据
// @auth                      （2020/04/05  20:22）
// @param     info             request.PageInfo
// @return    err              error
// @return    list             interface{}
// @return    total            int

func GetUserInfoList(info request.PageInfo) (err error, list interface{}, total int) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB
	var userList []model.SysUser
	err = db.Find(&userList).Count(&total).Error
	err = db.Limit(limit).Offset(offset).Find(&userList).Error
	return err, userList, total
}

/**
根据id获取用户
*/
func GetUserById(id uint) (err error, user model.SysUser) {
	err = global.GVA_DB.Where("id = ?", id).Find(&user).Error
	return err, user
}

/**
添加用户
*/
func AddUser(user model.SysUser) (error, model.SysUser) {
	// 否则 附加uuid 密码md5简单加密 注册
	user.Password = utils.MD5V([]byte(user.Password))
	user.UUID, _ = uuid.NewV4()

	err := global.GVA_DB.Create(&user).Error
	return err, user
}

/**
更新用户
*/
func UpdateUser(id uint, user model.SysUser) (error, model.SysUser) {
	// 否则 附加uuid 密码md5简单加密 注册
	user.Password = utils.MD5V([]byte(user.Password))
	err := global.GVA_DB.Where("id = ?", id).First(&model.SysUser{}).Update(&user).Error
	return err, user
}

/**
删除用户
*/
func DeleteUser(id uint) error {
	return global.GVA_DB.Where("id = ?", id).Delete(model.SysUser{}).Error
}
